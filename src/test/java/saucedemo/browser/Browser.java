package saucedemo.browser;

import org.openqa.selenium.WebDriver;
import saucedemo.driver.WebDriverHandler;
import saucedemo.sauceDemo;

public class Browser {

    private final WebDriver webDriver = WebDriverHandler.getWebDriver();

    public sauceDemo sauceDemo;

    public Browser() {
        sauceDemo = new sauceDemo();
    }

    public String getCurrentUrl(){
        return webDriver.getCurrentUrl();

    }

}
