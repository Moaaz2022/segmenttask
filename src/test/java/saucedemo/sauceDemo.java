package saucedemo;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import saucedemo.pages.*;

public class sauceDemo extends BasePage {
    @FindBy(id="user-name")
    private WebElement userName;

    @FindBy(id="password")
    private WebElement password;

    @FindBy(id="login-button")
    private WebElement loginButton;

    public WebElement getUSerName(){
        return userName;
    }

    public WebElement getPassword(){
        return password;
    }

    public void enterUserName(String userName) {
        getUSerName().sendKeys(userName);
    }

    public void enterPassword(String password) {
        getPassword().sendKeys(password);
    }

    public Products clickOnLoginButton(){
        loginButton.click();
        return new Products();
    }
    public Products products;
    public Cart cart;
    public Checkout checkout;
    public Overview overview;

}
