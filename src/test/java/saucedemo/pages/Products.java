package saucedemo.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import java.util.List;

public class Products extends BasePage{

    @FindBy(className = "btn_inventory")
    private List<WebElement> buttons;

    public List<WebElement> getButtons(){
        return buttons;
    }
    @FindBy(className = "title")
    private WebElement pageName;
    @FindBy(id="shopping_cart_container")
    private WebElement cartButton;
    @FindBy(className = "product_sort_container")
    private WebElement dropdownElement;

    @FindBy(className = "inventory_item_name")
    private List<WebElement> titleList;

    public List<WebElement> getTitleList(){
        return titleList;
    }

    public WebElement getDropdownElement(){
        return dropdownElement;
    }

    public void sortPrice(){
        Select dropdown = new Select(getDropdownElement());
        dropdown.selectByVisibleText("Price (high to low)");
    }





    public WebElement getPageName(){
        return pageName;
    }

    public void clickOnExpenciveItems(){
        getButtons().get(0).click();
        getButtons().get(1).click();
    }

    public Cart clickOnCartButton(){
        cartButton.click();
        return new Cart();
    }
}
