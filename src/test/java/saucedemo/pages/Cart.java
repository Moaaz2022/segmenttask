package saucedemo.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class Cart extends BasePage{
    @FindBy(id="checkout")
    private WebElement checkoutButton;
    @FindBy(className = "title")
    private WebElement pageName;

    @FindBy(className ="inventory_item_name")
    private List<WebElement> selectedProducts;

    public WebElement getPageName(){
        return pageName;
    }

    public List<WebElement> getSelectedProducts(){
        return selectedProducts;
    }

    public Checkout clickOnCheckoutButton(){
        checkoutButton.click();
        return new Checkout();
    }
}
