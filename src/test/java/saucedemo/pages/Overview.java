package saucedemo.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Overview extends BasePage{
    @FindBy(className = "title")
    private WebElement pageName;
    @FindBy(id = "finish")
    private WebElement finishButton;
    @FindBy(className = "complete-header")
    private WebElement thankYouMessage;
    @FindBy(className = "complete-text")
    private WebElement orderMessage;


    public WebElement getPageName(){
        return pageName;
    }
    public WebElement getFinishButton(){
        return finishButton;
    }
    public WebElement getThankYouMessage(){
        return thankYouMessage;
    }
    public WebElement getOrderMessage(){
        return orderMessage;
    }

    public void clickOnFinishButton(){
        finishButton.click();
    }
}
