package saucedemo.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Checkout extends BasePage{
    @FindBy(className = "title")
    private WebElement pageName;

    @FindBy(id="first-name")
    private WebElement firstName;

    @FindBy(id="last-name")
    private WebElement lastName;

    @FindBy(id="postal-code")
    private WebElement postalCode;

    @FindBy(id="continue")
    private WebElement continueButton;

    public WebElement getFirstName(){
        return firstName;
    }
    public WebElement getLastName(){
        return lastName;
    }
    public WebElement getPostalCode(){
        return postalCode;
    }

    public WebElement getPageName(){
        return pageName;
    }

    public void enterFirstName(String firstName) {
        getFirstName().sendKeys(firstName);
    }
    public void enterLastName(String lastName) {
        getLastName().sendKeys(lastName);
    }
    public void enterPostalCode(String postalCode) {
        getPostalCode().sendKeys(postalCode);
    }

    public Overview clickOnContinueButton(){
        continueButton.click();
        return new Overview();
    }



}
