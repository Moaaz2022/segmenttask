package saucedemo.tests;

import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import saucedemo.driver.WebDriverHandler;
import io.cucumber.java.*;
import saucedemo.utilities.extent_report.ExtentReport;
import saucedemo.utilities.image_reducer.ImageReducer;

import java.io.IOException;

public class hooksHandler extends BaseTest{

    @Before(order = 1)
    public void startTCHooks(Scenario scenario) {
        ExtentReport.setScenario(scenario);
        ExtentReport.startTC();
    }
    @Before(order = 2)
    public void setStepDefs() throws NoSuchFieldException, IllegalAccessException {
        ExtentReport.setStepDefs();
    }
    @AfterStep
    public void AfterStep(Scenario scenario) throws IOException {
        String stepName = ExtentReport.getCurrentStepName();
        com.aventstack.extentreports.Status logStatus;

        if (scenario.isFailed())
            logStatus = com.aventstack.extentreports.Status.FAIL;
        else
            logStatus = Status.PASS;
            try {
    byte[] image = ((TakesScreenshot) WebDriverHandler.getWebDriver()).getScreenshotAs(OutputType.BYTES);

        String base64Screenshot = ImageReducer.reduce(image, 800, 600);
        ExtentReport.getTest().log(logStatus, stepName, MediaEntityBuilder.createScreenCaptureFromBase64String(base64Screenshot).build());
            }catch (Exception e){
    ExtentReport.getTest().log(logStatus, stepName);
            }
    }

    @After()
    public void endTC() {
        if (ExtentReport.isCurrentlyUsingReport()) {
            ExtentReport.getExtent().flush();
        }
    }
}
