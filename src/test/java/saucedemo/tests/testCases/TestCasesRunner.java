package saucedemo.tests.testCases;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
        features = "src/test/java/saucedemo/tests/testCases",
        glue = {"saucedemo.tests"},
        plugin = {"html:reports/testcases-Report.html"},
        monochrome = true
)
public class TestCasesRunner extends AbstractTestNGCucumberTests {
}
