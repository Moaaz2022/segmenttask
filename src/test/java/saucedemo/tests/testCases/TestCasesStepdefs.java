package saucedemo.tests.testCases;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import saucedemo.browser.Browser;
import saucedemo.driver.WebDriverHandler;
import saucedemo.tests.BaseTest;
import org.testng.Assert;
import saucedemo.pages.BasePage;
import saucedemo.utilities.ConfigProperties;

import java.io.IOException;

import static io.restassured.RestAssured.given;


public class TestCasesStepdefs extends BaseTest {
    String firstProduct;
    String secondProduct;
    String token;
    private String url="WebsiteLink";
    @Given("website is opened")
    public void websiteIsOpened() throws IOException {
        webDriverHandler = new WebDriverHandler();
        browser = new Browser();
        configrationFile = new ConfigProperties("resources/configFiles/configrationFile.properties");
        url = configrationFile.getProperty(url);
        webDriverHandler.resetCache();
        webDriverHandler.navigateTo(url);
        BasePage.waitUntilPageIsLoaded();
    }
    @When("login with valid username {string} and password {string}")
    public void loginWithValidUsernameAndPassword(String userName,String password) {
    browser.sauceDemo.enterUserName(userName);
    browser.sauceDemo.enterPassword(password);
    browser.sauceDemo.products=browser.sauceDemo.clickOnLoginButton();
    }
    @Then("user logged in successfully and navigated to the products page")
    public void userLoggedInSuccessfullyAndNavigatedToTheProductsPage() {
       Assert.assertTrue(browser.sauceDemo.products.getPageName().isDisplayed());
    }
    @When("add the most expensive two products to your cart")
    public void addTheMostExpensiveTwoProductsToYourCart() {
        browser.sauceDemo.products.sortPrice();
        firstProduct=browser.sauceDemo.products.getTitleList().get(0).getText();
        secondProduct=browser.sauceDemo.products.getTitleList().get(1).getText();
        browser.sauceDemo.products.clickOnExpenciveItems();
    }
    @When("click on the cart button")
    public void clickOnTheCartButton() {
        browser.sauceDemo.cart=browser.sauceDemo.products.clickOnCartButton();
    }
    @Then("user navigated to cart page and the previously selected products are displayed at the page")
    public void userNavigatedToCartPageAndThePreviouslySelectedProductsAreDisplayedAtThePage() {
        Assert.assertTrue(browser.sauceDemo.cart.getPageName().isDisplayed());
        Assert.assertEquals(browser.sauceDemo.cart.getSelectedProducts().get(0).getText(),firstProduct);
        Assert.assertEquals(browser.sauceDemo.cart.getSelectedProducts().get(1).getText(),secondProduct);

    }
    @When("click on the checkout button")
    public void clickOnTheCheckoutButton() {
        browser.sauceDemo.checkout=browser.sauceDemo.cart.clickOnCheckoutButton();
    }
    @Then("user navigated to the checkout page")
    public void userNavigatedToTheCheckoutPage() {
        Assert.assertTrue(browser.sauceDemo.checkout.getPageName().isDisplayed());
    }
    @When("fill all the displayed form")
    public void fillAllTheDisplayedForm() {
        browser.sauceDemo.checkout.enterFirstName("Moaaz");
        browser.sauceDemo.checkout.enterLastName("Mohamed");
        browser.sauceDemo.checkout.enterPostalCode("02");
    }
    @When("click on the continue button")
    public void clickOnTheContinueButton() {
        browser.sauceDemo.overview=browser.sauceDemo.checkout.clickOnContinueButton();
    }
    @Then("user navigated to the overview page")
    public void userNavigatedToTheOverviewPage() {
        Assert.assertTrue(browser.sauceDemo.overview.getPageName().isDisplayed());
    }
    @Then("the URL matches with {string}")
    public void theURLMatchesWith(String url) {
        Assert.assertEquals(browser.getCurrentUrl(),url);
    }
    @When("click on the finish button")
    public void clickOnTheFinishButton() {
        browser.sauceDemo.overview.clickOnFinishButton();
    }
    @Then("thank you message should be displayed as {string}")
    public void thankYouMessageShouldBeDisplayedAs(String message) {
        Assert.assertEquals(browser.sauceDemo.overview.getThankYouMessage().getText(),message);
    }
    @Then("order has been dispatched message should be displayed as {string}")
    public void orderHasBeenDispatchedMessageShouldBeDisplayedAs(String message) {
        Assert.assertEquals(browser.sauceDemo.overview.getOrderMessage().getText(),message);
        webDriverHandler.resetCache();
        WebDriverHandler.close();
    }


    private Response response;

    @Given("the base URL is {string} and user has token")
    public void setBaseURLAndUserHasToken(String baseUrl) {
        RestAssured.baseURI = baseUrl;
        String requestBody = "{\n" +
                "   \"clientName\": \"sgssaaan\",\n" +
                "   \"clientEmail\": \"tess@eplefsaafsa.com\"\n" +
                "}";

        Response response = RestAssured.given()
                .baseUri(baseUrl)
                .basePath("/api-clients/")
                .contentType(ContentType.JSON)
                .body(requestBody)
                .when()
                .post();
        token = response.jsonPath().getString("accessToken");
    }

    @When("I send a GET request to {string}")
    public void sendGetRequest(String endpoint) {
        response = RestAssured.when().get(endpoint);
    }

    @When("I send a POST request to {string} with the following request body:")
    public void sendPostRequest(String endpoint, String requestBody) {
        response = RestAssured.given()
                .contentType(ContentType.JSON)
                .body(requestBody)
                .header("Authorization", "Bearer " + token)
                .when()
                .post(endpoint);
    }

    @When("I send a PATCH request to {string} with the following request body:")
    public void sendPatchRequest(String endpoint, String requestBody) {
        response = RestAssured.given()
                .contentType(ContentType.JSON)
                .body(requestBody)
                .header("Authorization", "Bearer " + token)
                .when()
                .patch(endpoint);
    }

    @When("I send a DELETE request to {string}")
    public void sendDeleteRequest(String endpoint) {
         response = RestAssured.given()
                .basePath(endpoint)
                .contentType(ContentType.JSON)
                .header("Authorization", "Bearer " + token)
                .when()
                .delete();
    }

    @Then("the response status code should be {int}")
    public void verifyStatusCode(int expectedStatusCode) {
        Assert.assertEquals(response.getStatusCode(),expectedStatusCode);
    }
}
