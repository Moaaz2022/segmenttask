Feature: TestCases

  Scenario Outline: Test Case 1
    Given website is opened
    When login with valid username "<userName>" and password "<password>"
    Then user logged in successfully and navigated to the products page
    When add the most expensive two products to your cart
    And click on the cart button
    Then user navigated to cart page and the previously selected products are displayed at the page
    When click on the checkout button
    Then user navigated to the checkout page
    When fill all the displayed form
    And click on the continue button
    Then user navigated to the overview page
    And the URL matches with "<url>"
    When click on the finish button
    Then thank you message should be displayed as "<thankYouMessage>"
    And order has been dispatched message should be displayed as "<orderMessage>"

    Examples:
      | userName      | password       | url                                             | thankYouMessage            | orderMessage                                                                            |
      | standard_user | secret_sauce   |https://www.saucedemo.com/checkout-step-two.html | Thank you for your order!  | Your order has been dispatched, and will arrive just as fast as the pony can get there! |


  Scenario: TestCase 2
    Given the base URL is "https://simple-books-api.glitch.me/" and user has token
    When I send a GET request to "/books"
    Then the response status code should be 200
    When I send a POST request to "/orders" with the following request body:
      """
      {
        "bookId": 1,
        "customerName": "John"
      }
      """
    Then the response status code should be 201
    When I send a PATCH request to "/orders/PF6MflPDcuhWobZcgmJy5" with the following request body:
      """
      {
        "newData": "updated data"
      }
      """
    Then the response status code should be 404
    When I send a DELETE request to "/orders/PF6MflPDcuhWobZcgmJy5"
    Then the response status code should be 404
