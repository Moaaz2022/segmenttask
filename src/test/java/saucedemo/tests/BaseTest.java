package saucedemo.tests;

import saucedemo.browser.Browser;
import saucedemo.utilities.ConfigProperties;
import saucedemo.driver.WebDriverHandler;

public class BaseTest {
    protected WebDriverHandler webDriverHandler;
    protected static Browser browser;
    protected static ConfigProperties ratePlans;
    protected static ConfigProperties configrationFile;
    protected static boolean removeCookiesPopup;
}